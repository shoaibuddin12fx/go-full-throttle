import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.gofullthrottle.app',
  appName: 'Go Full Throttle',
  webDir: 'www',
  bundledWebRuntime: false,
  server: { iosScheme: 'ionic' },
  plugins: {
    SplashScreen: {
      launchShowDuration: 3000,
      launchAutoHide: false,
      backgroundColor: '#000000',
      androidSplashResourceName: 'splash',
      androidScaleType: 'CENTER_CROP',
      showSpinner: false,
      androidSpinnerStyle: 'large',
      iosSpinnerStyle: 'small',
      spinnerColor: '#FFFFFF',
      splashFullScreen: true,
      splashImmersive: true,
      layoutName: 'launch_screen',
      useDialog: true,
    },
    GoogleAuth: {
      scopes: ['profile', 'email'],
      clientId:
        '850465536285-hi3lc1igf7ogi9frs1cou9e3bvtf98oi.apps.googleusercontent.com',
    },
  },
};

export default config;
