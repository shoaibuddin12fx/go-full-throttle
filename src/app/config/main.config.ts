class cons {
  static secure = '';
  static domain = 'gft.waprojects.space';
  static default_part = 'api';
}

export const Config = {
  SERVICEURL: `http${cons.secure}://${cons.domain}/${cons.default_part}`,
};

export const firebaseConfig = {
  apiKey: 'AIzaSyAYODDcGaRFgyZtoDuObE_GLk2Z16xjfrA',
  authDomain: 'go-full-throttle-c7142.firebaseapp.com',
  projectId: 'go-full-throttle-c7142',
  storageBucket: 'go-full-throttle-c7142.appspot.com',
  messagingSenderId: '850465536285',
  appId: '1:850465536285:web:c2661ba55f7752579d49f4',
  measurementId: 'G-VQB2NDQG7L',
};
