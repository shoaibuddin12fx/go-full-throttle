import { Injectable } from '@angular/core';
const races = require('src/app/data/races.json');
const slides = require('src/app/data/slides.json');
const news = require('src/app/data/news.json');
const race_events = require('src/app/data/race_events.json');
const drivers = require('src/app/data/drivers.json');
const racing_news = require('src/app/data/racing-news.json');

@Injectable({
  providedIn: 'root',
})
export class DataService {
  races_data: any = [];
  user: any;
  race_data;
  isPopular = false;
  driver_data: any;
  news_data: any;
  getRacingSeries() {
    return races;
  }

  getRacingSeriesById(id) {
    return races.find((x) => x.id == id);
  }

  getHomeSlides() {
    return slides;
  }

  getNews() {
    return news;
  }

  getRacingNews() {
    return racing_news;
  }

  getDrivers() {
    return drivers;
  }

  getRaceEvents(id) {
    return race_events.filter((x) => parseInt(x.race_id) == parseInt(id));
  }

  getRaceEventById(id) {
    return race_events.find((x) => x.id == id);
  }

  getDriversByRaceId(id) {
    return race_events
      .find((x) => x.id == id)
      .positions.map((x) =>
        this.getDrivers().filter((driver) => driver.id == x.driver_id)
      );
  }

  getDriverById(id) {
    return drivers.find((x) => x.id == id);
  }

  getDriversById(id) {
    return drivers.filter((x) => x.id != id);
  }

  getPosition(race_id, driver_id) {
    return race_events
      .find((x) => x.id == race_id)
      .positions.find((x) => x.driver_id == driver_id);
  }

  getDriverData() {
    return this.driver_data;
  }
}
