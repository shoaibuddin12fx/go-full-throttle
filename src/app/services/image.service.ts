import { Injectable } from '@angular/core';
import {
  Camera,
  CameraResultType,
  CameraSource,
  ImageOptions,
  GalleryImageOptions,
} from '@capacitor/camera';

@Injectable({
  providedIn: 'root',
})
export class ImageService {
  file: File;
  constructor() {}

  pickImages() {
    return new Promise((resolve) => {
      const galleryOptions: GalleryImageOptions = {
        width: 200,
      };

      Camera.pickImages(galleryOptions).then(
        async (imageData) => {
          let blobs = [];
          imageData.photos.forEach(async (x) => {
            let blob = await fetch(x.webPath).then((res) => res.blob());
            blobs.push(blob);
            // .catch((error) => {
            //   console.log('Fetch error', error);
            // });
          });

          resolve({ isBase64: false, blobs });

          // let res = await this.makeFileIntoBlob(imageData.photos[0].path);
          // console.log('makeFileIntoBlob', res);
        },
        (err) => {
          resolve(null);
        }
      );
    });
  }

  openCamera() {
    return new Promise((res) => {
      console.log('Opening Camera');

      const cameraOptions: ImageOptions = {
        width: 200,
        resultType: CameraResultType.Base64,
        source: CameraSource.Camera,
      };

      Camera.getPhoto(cameraOptions).then(
        (imageData) => {
          res({ ...imageData, isBase64: true });
        },
        (err) => {
          console.error('Camera.getPhoto', err);

          res(null);
        }
      );
    });
  }
  //open galary picture
  // openGalary() {
  //   return new Promise<string>((res) => {
  //     const cameraOptions: ImageOptions = {
  //       height: 150,
  //       width: 150,
  //       allowEditing: true,
  //       quality: 50,
  //       resultType: CameraResultType.Uri,
  //       source: CameraSource.Photos,
  //     };
  //     Camera.getPhoto(cameraOptions).then(
  //       (imageData) => {
  //         console.log('imageData', imageData);
  //         res(imageData.base64String);
  //       },
  //       (err) => {
  //         res(null);
  //       }
  //     );
  //   });
  // }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    return new Promise(async (resolve) => {
      const byteCharacters = atob(b64Data);
      const byteArrays = [];

      for (
        let offset = 0;
        offset < byteCharacters.length;
        offset += sliceSize
      ) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }

      const blob = new Blob(byteArrays, { type: contentType });
      resolve(blob);
    });
  }

  // FILE STUFF
  //   makeFileIntoBlob(_imagePath) {
  //     // INSTALL PLUGIN - cordova plugin add cordova-plugin-file
  //     return new Promise((resolve, reject) => {
  //       let fileName = '';
  //       this.file
  //         .resolveLocalFilesystemUrl(_imagePath)
  //         .then((fileEntry) => {
  //           let { name, nativeURL } = fileEntry;

  //           // get the path..
  //           let path = nativeURL.substring(0, nativeURL.lastIndexOf('/'));
  //           console.log('path', path);
  //           console.log('fileName', name);

  //           fileName = name;

  //           // we are provided the name, so now read the file into
  //           // a buffer
  //           return this.file.readAsArrayBuffer(path, name);
  //         })
  //         .then((buffer) => {
  //           // get the buffer and make a blob to be saved
  //           let imgBlob = new Blob([buffer], {
  //             type: 'image/jpeg',
  //           });
  //           console.log(imgBlob.type, imgBlob.size);
  //           resolve({
  //             fileName,
  //             imgBlob,
  //           });
  //         })
  //         .catch((e) => {
  //           console.log('makeFileIntoBlob Error:', e);
  //           reject(e);
  //         });
  //     });
  //   }

  getUserImage(path) {
    return `https://veenmeapi.thesupportonline.net/uploads/profile-images/${path}`;
  }
}
