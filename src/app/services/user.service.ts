import { Injectable } from '@angular/core';
const users = require('src/app/data/users.json');
@Injectable({
  providedIn: 'root',
})
export class UserService {
  user = null;
  constructor() {}

  setSignupUserInLocal(userdata) {
    return new Promise((resolve) => {
      // check user in mobile local storage

      console.log(userdata);
    });
  }

  login(formdata) {
    return new Promise((resolve) => {
      let record = this.getTempUser();
      if (record) this.setToken(record['id']);
      resolve(record);
      //let record = users.find((x) => x.email == formdata['email']);
      // if (record) {
      //   this.setToken(record['id']);
      //   this.user = record;
      //   resolve(record);
      // } else {
      //   this.user = null;
      //   resolve(false);
      // }
    });
  }

  getUser() {
    return new Promise(async (resolve) => {
      let token = await this.getToken();
      console.log(token);
      let record = users.find((x) => parseInt(x.id) == parseInt(token));
      if (record) {
        this.user = record;
        resolve(record);
      } else {
        this.user = null;
        resolve(false);
      }
    });
  }

  getTempUser() {
    return users[0];
  }

  setToken(token) {
    return localStorage.setItem('token', token);
  }

  setLocalUser(user) {
    return localStorage.setItem('user', JSON.stringify(user));
  }

  getLocalUser() {
    return new Promise((resolve) => {
      let user = localStorage.getItem('user');
      if (user && user !== '') resolve(JSON.parse(user));
      else resolve(null);
    });
  }

  removeToken() {
    return localStorage.removeItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }
}
