import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from './api.service';
import { EventsService } from './basic/events.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root',
})
export class NetworkService {
  constructor(
    public utility: UtilityService,
    public api: ApiService,
    public router: Router,
    private events: EventsService
  ) {
    // console.log('Hello NetworkProvider Provider');
  }

  signup(obj) {
    return this.httpPostResponse('signup', obj);
  }

  socialLogin(obj) {
    return this.httpPostResponse('socialLogin', obj);
  }

  getConstructors() {
    return this.httpGetResponse('constructors.json');
  }

  getSeasons() {
    return this.httpGetResponse('get_all_seasons'); //'seasons.json?limit=100');
  }

  getSheduleBySeason(season) {
    return this.httpGetResponse(`get_schedule_by_season?year=${season}`);
  }

  getSchedules() {
    return this.httpGetResponse('current.json');
  }

  getRaceResults(season, round) {
    return this.httpGetResponse(
      `get_race_result?year=${season}&round=${round}`
    );
  }

  getRaceDrivers(season, round) {
    return this.httpGetResponse(
      `get_race_drivers?year=${season}&round=${round}`
    );
  }

  getDrivers() {
    return this.httpGetResponse(`list_of_all_drivers`);
  }

  getNascarDrivers() {
    return this.httpGetResponse(`list_of_nascar_drivers`);
  }

  getRaceSchedultByData(season, round) {
    return this.httpGetResponse(
      `get_schedule_by_season_round?year=${season}&round=${round}`
    );
  }

  login(data) {
    return this.httpPostResponse(`login`, data);
  }

  updateProfile(data) {
    return this.httpPostResponse(`updateProfile`, data);
  }

  getNews(type) {
    return this.httpGetResponse(`getNews?type=${type}`);
  }
  getAppNotifications() {
    return this.httpGetResponse(`getAppNotifications`);
  }

  getNascarSchedule(season) {
    return this.httpGetResponse(`get_nascar_schedule_by_season?year=${season}`);
  }

  getNascarRaceResult(race_id) {
    return this.httpGetResponse(`get_nascar_race_result?race_id=${race_id}`);
  }

  getRecentSchedule() {
    return this.httpGetResponse('get_recent_schedule');
  }

  getNascarRecentSchedule() {
    return this.httpGetResponse('get_nascar_recent_schedule');
  }
  // getDrivers() {
  //   return this.httpGetResponse('drivers.json');
  // }

  serialize = (obj) => {
    const str = [];
    for (const p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
    }
    return str.join('&');
  };

  httpPostResponse(
    key,
    data,
    id = null,
    showloader = false,
    showError = true,
    contenttype = 'application/json'
  ) {
    return this.httpResponse(
      'post',
      key,
      data,
      id,
      showloader,
      showError,
      contenttype
    );
  }

  httpGetResponse(
    key,
    id = null,
    showloader = false,
    showError = true,
    contenttype = 'application/json'
  ) {
    return this.httpResponse(
      'get',
      key,
      {},
      id,
      showloader,
      showError,
      contenttype
    );
  }

  httpPutResponse(
    key,
    data,
    id = null,
    showloader = false,
    showError = true,
    contenttype = 'application/json'
  ) {
    return new Promise((resolve, reject) => {
      id = id ? `/${id}` : '';
      const url = key + id;

      this.api.put(key, data).subscribe((res: any) => {
        if (res.bool !== true) {
          if (showError) {
            this.utility.presentSuccessToast(res.message);
          }
          reject(null);
        } else {
          resolve(res);
        }
      });
    });
  }

  httpPatchResponse(
    key,
    data,
    id = null,
    showloader = false,
    showError = true,
    contenttype = 'application/json'
  ) {
    return new Promise((resolve, reject) => {
      id = id ? `/${id}` : '';
      const url = key + id;

      this.api.patch(key, data).subscribe((res: any) => {
        if (res.bool !== true) {
          if (showError) {
            this.utility.presentSuccessToast(res.message);
          }
          reject(null);
        } else {
          resolve(res);
        }
      });
    });
  }

  httpDeleteResponse(
    key,
    data,
    id = null,
    showloader = false,
    showError = true,
    contenttype = 'application/json'
  ) {
    return new Promise((resolve, reject) => {
      this.api.delete(key).subscribe((res: any) => {
        console.log(res);
        if (res.bool !== true) {
          if (showError) {
            this.utility.presentSuccessToast(res.message);
          }
          reject(null);
        } else {
          resolve(res);
        }
      });
    });
  }

  // default 'Content-Type': 'application/json',
  httpResponse(
    type = 'get',
    key,
    data,
    id = null,
    showloader = false,
    showError = true,
    contenttype = 'application/json'
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      if (showloader === true) {
        this.utility.showLoader();
      }

      id = id ? '/' + id : '';
      const url = key + id;

      const seq =
        type === 'get' ? this.api.get(url, {}) : this.api.post(url, data);

      seq.subscribe(
        (res: any) => {
          if (showloader === true) {
            this.utility.hideLoader();
          }

          // if (res.success !== true) {
          //   if (showError) {
          //     this.utility.presentSuccessToast(res.message);
          //   }
          //   reject(null);
          // } else {
          resolve(res);
          //}
        },
        (err) => {
          const error = err;
          console.log('API_ERROR', err);
          resolve(err);

          if (showloader === true) {
            this.utility.hideLoader();
          }

          if (showError) {
            this.utility.presentFailureToast(error?.err.toString());
          }

          console.log(err);

          // if(err.status === 401){
          //   this.router.navigate(['splash']);
          // }
        }
      );
    });
  }

  showFailure(err) {
    // console.error('ERROR', err);
    err = err ? err.message : 'check logs';
    this.utility.presentFailureToast(err);
  }
}
