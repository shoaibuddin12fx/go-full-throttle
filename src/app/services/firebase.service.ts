// import { Injectable } from '@angular/core';
// import { AngularFireAuth } from '@angular/fire/compat/auth';
// import { AngularFireDatabase } from '@angular/fire/compat/database';
// import { AngularFireStorage } from '@angular/fire/compat/storage';
// import {
//   AngularFirestore,
//   AngularFirestoreDocument,
// } from '@angular/fire/compat/firestore';
// import { UtilityService } from './utility.service';
// import { GoogleAuthProvider } from '@angular/fire/auth';

// @Injectable({
//   providedIn: 'root',
// })
// export class FirebaseService {
//   public user;
//   constructor(
//     public fireAuth: AngularFireAuth,
//     public firebase: AngularFireDatabase,
//     public fireStorage: AngularFireStorage,
//     public afStore: AngularFirestore,
//     public utility: UtilityService
//   ) {}

//   getCurrentUser() {
//     return new Promise<any>(async (res) => {
//       var user = await this.fireAuth.currentUser;
//       if (user && user.uid) {
//         var ref = this.getDatabase().ref('/users/' + user['uid']);
//         ref.on('value', (_snapshot: any) => {
//           let snapshot = _snapshot.val();
//           if (snapshot) {
//             this.user = snapshot;
//           } else {
//             this.user = null;
//           }
//         });
//       } else {
//         res(null);
//       }
//     });
//   }

//   // Auth logic to run auth providers
//   googleLogin() {
//     return new Promise<any>((res) => {
//       this.fireAuth
//         .signInWithPopup(new GoogleAuthProvider())
//         .then((result) => {

//           res(result.user);
//         })
//         .catch((error) => {
//           console.log(error);
//           res(null);
//         });
//     });
//   }

//   async SignIn(email, password) {
//     return new Promise(async (res) => {
//       console.log('running here');
//       let self = this;
//       await this.fireAuth
//         .signInWithEmailAndPassword(email, password)
//         .then(async (value) => {
//           let _user = await this.getUser();
//           res(_user);
//         })
//         .catch((reason) => {
//           console.log('here is the reason', reason);
//           self.utility.presentToast(reason);
//           res(null);
//         });
//     });
//   }

//   RegisterUser(email, password) {
//     return this.fireAuth.createUserWithEmailAndPassword(email, password);
//   }

//   SetUserData(user, userData) {
//     const userRef: AngularFirestoreDocument<any> = this.afStore.doc(
//       `users/${user.uid}`
//     );
//     return userRef.set(userData, {
//       merge: true,
//     });
//   }

//   getUserData() {
//     return new Promise(async (res, rej) => {
//       let user = await this.getUser();
//       console.log('USER_FB', user);

//       if (!user) {
//         res(null);
//       }

//       this.afStore
//         .doc(`users/${user['uid']}`)
//         .valueChanges()
//         .subscribe(
//           (snapshot) => {
//             console.log('USER_CHANGES', snapshot);
//             res(snapshot);
//           },
//           (error) => {
//             alert(error.toString());
//             res(null);
//           }
//         );
//     });
//   }

//   getUser() {
//     return new Promise((res) => {
//       this.fireAuth.user.subscribe(
//         (_user) => {
//           res(_user);
//         },
//         (error) => {
//           res(null);
//         }
//       );
//     });

//     //  return this.ngFireAuth.getCurrentUser();
//   }

//   getDatabase() {
//     return this.firebase.database;
//   }

//   async signOut() {
//     await this.fireAuth.signOut();
//   }
// }
