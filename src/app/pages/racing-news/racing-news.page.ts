import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-racing-news',
  templateUrl: './racing-news.page.html',
  styleUrls: ['./racing-news.page.scss'],
})
export class RacingNewsPage extends BasePage implements OnInit {
  constructor(injector: Injector) {
    super(injector);
  }

  slides = [];
  news = [];
  races = [];

  ngOnInit() {}

  ionViewDidEnter() {
    // this.modals.present(PromotionsComponent, {}, 'transparent-banner');
    this.slides = this.dataService.getHomeSlides();
    this.news = this.dataService.getRacingNews();
    this.races = this.dataService.getRacingSeries();
  }

  raceEventDetail(id) {
    this.nav.push(`pages/news`, { type: id });
  }

  // watchVideo() {
  //   this.videoPlayer
  //     .play('https://www.youtube.com/watch?v=bHWgc5MPnPA')
  //     .then(() => {
  //       console.log('video completed');
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // }
}
