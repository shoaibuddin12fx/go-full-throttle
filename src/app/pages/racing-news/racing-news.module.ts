import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RacingNewsPageRoutingModule } from './racing-news-routing.module';

import { RacingNewsPage } from './racing-news.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { HomeNewsModule } from '../home/home-news/home-news.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RacingNewsPageRoutingModule,
    HeaderModule,
    HomeNewsModule,
  ],
  declarations: [RacingNewsPage],
})
export class RacingNewsPageModule {}
