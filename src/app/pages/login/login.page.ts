import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
// import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { GooglePlus } from '@awesome-cordova-plugins/google-plus/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage implements OnInit {
  aForm: FormGroup;
  loading = false;

  constructor(injector: Injector, private googlePlus: GooglePlus) {
    super(injector);
    this.initializeApp();
  }

  ngOnInit() {
    this.setupForm();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // GoogleAuth.initialize({
      //   clientId:
      //     '850465536285-fuknht138vpnlmq2m6uhn1ebqkgf1v91.apps.googleusercontent.com',
      //   scopes: ['profile', 'email'],
      //   grantOfflineAccess: true,
      // });
    });
  }

  setupForm() {
    const re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      email: [
        '',
        // 'john_smith22@gmail.com',
        Validators.compose([Validators.required, Validators.email]),
      ],
      password: [
        '',
        // '123456',
        Validators.compose([
          Validators.minLength(6),
          Validators.maxLength(30),
          Validators.required,
        ]),
      ],
    });
  }

  openSignup() {
    this.nav.navigateTo('pages/signup');
  }

  async login() {
    if (!this.aForm.valid) {
      this.utility.presentFailureToast('Pleae fill all fields properly');
      return;
    }

    const formdata = this.aForm.value;
    this.loading = true;
    let res = await this.network.login(formdata);
    console.log('LOGIN_RES', res);
    if (res && !res.errors) {
      if (res.user) {
        let user = res.user;
        this.users.setLocalUser(user);
        this.nav.push('pages/tabbar');
      } else this.utility.presentFailureToast(res.message);
    } else {
      let passError = res.errors?.password?.toString();
      let emailError = res.errors?.email?.toString();
      this.utility.presentFailureToast(passError ?? emailError);
    }

    // let email = this.getValue('email');
    // let password = this.getValue('password');
    // let user = await this.firebase.SignIn(email, password);
    // this.handleUser(user);

    this.loading = false;
  }

  // async loginWithGoogle() {
  //   this.googleLogin();
  // }

  async loginWithGoogle() {
    console.log('loginWithGoogle');

    try {
      const resResult = await this.googlePlus.login({}); // { user: null };
      console.log('loginWithGoogle', resResult);

      if (resResult && resResult.email) {
        let data = {
          name: resResult['displayName']
            ? resResult['displayName']
            : resResult['userId'],
          email: resResult['email'],
        };
        this.socialLogin(data);
        // alert('Login Success');
        // let res = {
        //   user: {
        //     displayName: resResult['displayName']
        //       ? resResult['displayName']
        //       : resResult['userId'],
        //     email: resResult['email'],
        //     uid: resResult['userId'],
        //   },
        // };
        // alert(JSON.stringify(res));

        //  this.signUpwithSocial(res, 'google');
      }
    } catch (err) {
      console.error(err);
      alert(err);
    }
  }

  async socialLogin(data) {
    let res = await this.network.socialLogin(data);
    console.log('socialLogin', res);
    if (res) {
      if (!res.error && res.data) {
        let user = res.data.user;
        this.users.setLocalUser(user);
        this.nav.push('pages/tabbar');
      } else
        this.utility.presentFailureToast(
          res.error?.message ?? 'Something went wrong'
        );
    }
  }

  // async googleLogin() {
  //   GoogleAuth.signIn()
  //     .then((user) => {
  //       console.log('Google_Login', user);
  //       alert('Login Success');
  //       if (user) this.handleUser(user);
  //     })
  //     .catch((err) => {
  //       console.log('====================================');
  //       console.log('Google Login Error', err);
  //       alert(`Login Failed: ${JSON.stringify(err)}`);
  //       console.log('====================================');
  //     });
  // }

  handleUser(user) {
    console.log('Handle User', user);
    if (user) {
      localStorage.setItem('user', JSON.stringify(user));
      this.nav.push('pages/tabbar');
    }
  }

  getValue(prop) {
    return this.aForm.get(prop).value;
  }

  setValue(prop, value) {
    this.getValue(prop).setValue(value);
  }
}
