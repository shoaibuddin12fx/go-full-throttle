import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.page.html',
  styleUrls: ['./car-detail.page.scss'],
})
export class CarDetailPage extends BasePage implements OnInit {
  item;
  car;
  cars;
  list = ['1', '2'];
  drivers;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  driverDetails(driver) {
    this.nav.push('pages/driver-detail', driver);
  }
}
