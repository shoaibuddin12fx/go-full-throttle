import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { SplashScreen } from '@capacitor/splash-screen';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage extends BasePage implements OnInit {
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    SplashScreen.hide();
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.initialize();
    }, 1000);
  }

  async initialize() {
    // check if firebase auth already set for user
    SplashScreen.hide();

    let res = await this.users.getLocalUser();

    setTimeout(() => {
      if (res) {
        // send to dashboard
        this.nav.push('pages/tabbar');
      } else {
        this.nav.push('pages/login');
      }
    }, 3000);
  }
}
