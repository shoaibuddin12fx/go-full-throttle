import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeasonsPageRoutingModule } from './seasons-routing.module';

import { SeasonsPage } from './seasons.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { TypeSelectionModule } from 'src/app/components/type-selection/type-selection.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeasonsPageRoutingModule,
    HeaderModule,
    TypeSelectionModule,
  ],
  declarations: [SeasonsPage],
})
export class SeasonsPageModule {}
