import { Component, Injector, OnInit } from '@angular/core';
import { TypeSelectionComponent } from 'src/app/components/type-selection/type-selection.component';
import { SELECTION_TYPE } from 'src/app/const/enums';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-seasons',
  templateUrl: './seasons.page.html',
  styleUrls: ['./seasons.page.scss'],
})
export class SeasonsPage extends BasePage implements OnInit {
  seasons: [];
  season;
  index = 0;
  type: any;
  constructor(injector: Injector) {
    super(injector);
  }

  async ngOnInit() {
    let data = await this.modals.present(TypeSelectionComponent);
    this.typeSelected(data?.data?.type);
  }

  async getSeasons() {
    let res = await this.network.getSeasons();
    console.log('getSeasons', res["0"]);
    this.seasons =
      res["data"]?.sort((a, b) => {
        return parseInt(b.year) - parseInt(a.year);
      }) ?? [];
  }

  async seasonSelected() {
    this.season = this.seasons[this.index]['year'];
    this.nav.push('pages/races', { type: this.type, season: this.season });
  }

  typeSelected(type) {
    console.log('TYPE', type);
    this.type = type;

    //if (type === SELECTION_TYPE.FORMULA_1)
      this.getSeasons();
  }

  changeYear(add) {
    if (add && this.index < this.seasons.length) this.index++;
    else if (this.index > 0) this.index--;
  }
}
