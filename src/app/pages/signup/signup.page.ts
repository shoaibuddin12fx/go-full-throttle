import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { StringsService } from 'src/app/services/basic/strings.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage extends BasePage implements OnInit {
  signupObj = {
    name: '',
    email: '',
    password: '',
  };

  loading = false;

  aForm: FormGroup;

  constructor(injector: Injector, private strings: StringsService) {
    super(injector);
    this.setupForm();
  }

  setupForm() {
    const re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      name: [
        '',
        Validators.compose([
          // Validators.minLength(3),
          // Validators.maxLength(30),
          // Validators.pattern('[a-zA-Z ]*'),
          Validators.required,
        ]),
      ],
      email: [
        // 'johncorbor1@email.com',
        '',
        Validators.compose([Validators.required, Validators.email]),
      ],
      // phone: [
      //   //'123456789',
      //   '',
      //   Validators.compose([Validators.required]),
      // ],
      password: [
        //'123456',
        '',
        Validators.compose([
          // Validators.minLength(6),
          // Validators.maxLength(30),
          Validators.required,
        ]),
      ],
    });
  }

  ngOnInit() {}

  async singUp() {
    if (!this.aForm.valid) {
      this.utility.presentFailureToast('Pleae fill all fields properly');
      return;
    }
    this.onSignup();
    this.loading = false;
  }

  onTelephoneChange(ev) {
    if (ev.inputType !== 'deleteContentBackward') {
      const utel = this.utility.onkeyupFormatPhoneNumberRuntime(
        ev.target.value,
        false
      );
      console.log(utel);
      ev.target.value = utel;
      this.aForm.controls['phone'].patchValue(utel);
      // ev.target.value = utel;
    }
  }

  openLogin() {
    this.nav.pop();
  }

  async onSignup() {
    this.loading = true;
    // let name = this.getValue('name');
    // let email = this.getValue('email');
    // let password = this.getValue('password');

    // var obj = {
    //   name: name,
    //   email: email,
    //   password: password,
    // };
    let obj = this.aForm.value;

    console.log(obj);

    const res = await this.network.signup(obj);
    console.log('onSignup', res);

    if (res) {
      if (!res.error && res.data) {
        let user = res.data.user;
        this.users.setLocalUser(user);
        this.nav.push('pages/tabbar');
      } else
        this.utility.presentFailureToast(
          res.error?.message ?? 'Something went wrong'
        );
    }
    console.log(res);

    // this.firebase
    //   .RegisterUser(email, password)
    //   .then(async (success) => {
    //     console.log('Signup', success);
    //     let user = await this.firebase.getUser();
    //     this.loading = false;
    //     if (user) {
    //       //alert('Success');
    //       console.log('user', user);
    //       this.setUserData(user);
    //     } else this.utility.presentToast('Error occured');
    //   })
    //   .catch((reason) => {
    //     this.loading = false;
    //     this.utility.presentFailureToast(reason.toString());
    //   });
  }

  // async setUserData(user) {
  //   let obj = this.aForm.value;
  //   this.loading = true;
  //   const res = await this.firebase.SetUserData(user, {
  //     ...obj,
  //     gender: 'male',
  //   });
  //   this.loading = false;
  //   console.log('setUserData', res);
  //   this.nav.push('pages/tabbar');
  // }

  getValue(prop) {
    return this.aForm.get(prop).value;
  }

  setValue(prop, value) {
    this.getValue(prop).setValue(value);
  }
}
