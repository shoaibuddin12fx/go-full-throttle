import { Component, Injector, OnInit } from '@angular/core';
import { ViewDidEnter, ViewWillEnter } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage extends BasePage implements OnInit, ViewWillEnter {
  user;
  profilePhoto = 'assets/user.png';

  menu = [
    {
      id: 1,
      icon: 'assets/menu/user.png',
      label: 'Edit Profile',
      link: 'pages/edit-profile',
    },
    {
      id: 2,
      icon: 'assets/menu/uhead.png',
      label: 'Support',
      link: 'pages/support',
    },
    {
      id: 3,
      icon: 'assets/menu/info.png',
      label: 'About Us',
      link: 'pages/about-us',
    },
    {
      id: 4,
      icon: 'assets/menu/terms.png',
      label: 'Terms & Conditions',
      link: 'pages/terms-conditions',
    },
    {
      id: 5,
      icon: 'assets/menu/review.png',
      label: 'My Reviews',
      link: 'pages/reviews',
    },
    // {
    //   id: 6,
    //   icon: 'assets/menu/language.png',
    //   label: 'Change Language',
    //   link: '',
    // },
    // {
    //   id: 7,
    //   icon: 'assets/menu/share.png',
    //   label: 'Share App',
    //   link: '',
    // },
    // {
    //   id: 8,
    //   icon: 'assets/menu/like.png',
    //   label: 'Rate App',
    //   link: 'pages/rating',
    // },
  ];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  ionViewWillEnter(): void {
    this.initialize();
  }

  async initialize() {
    // this.utility.showLoader();
    this.user = await this.users.getLocalUser();

    this.dataService.user = this.user;
    if (this.user['profile_image'])
      this.profilePhoto = `http://gft.waprojects.space/images/${this.user['profile_image']}`;
    // this.utility.hideLoader();
  }

  openLink(item) {
    console.log(item);
    if (item != undefined) {
      switch (item.id) {
        case 1:
          this.nav.push(item.link);
          break;
        default:
          break;
      }
      if (item.link && item.link !== '') this.nav.push(item.link);
      else this.nav.push('pages/edit-profile');
    }
  }

  async logout() {
    // let res = await this.firebase.signOut();
    this.nav.push('pages/login');
  }
}
