import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.page.html',
  styleUrls: ['./news-detail.page.scss'],
})
export class NewsDetailPage extends BasePage implements OnInit {
  news;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.news = this.dataService.news_data;
  }
}
