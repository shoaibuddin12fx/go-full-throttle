import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NascarRaceItemComponent } from './nascar-race-item.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [NascarRaceItemComponent],
  imports: [CommonModule, IonicModule],
  exports: [NascarRaceItemComponent],
})
export class NascarRaceItemModule {}
