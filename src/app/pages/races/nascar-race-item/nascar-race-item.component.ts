import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'nascar-race-item',
  templateUrl: './nascar-race-item.component.html',
  styleUrls: ['./nascar-race-item.component.scss'],
})
export class NascarRaceItemComponent implements OnInit {
  @Input() item;
  constructor() {}

  ngOnInit() {}
}
