import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RaceItemComponent } from './race-item.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [RaceItemComponent],
  imports: [CommonModule, IonicModule],
  exports: [RaceItemComponent],
})
export class RaceItemModule {}
