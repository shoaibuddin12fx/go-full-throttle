import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-race-item',
  templateUrl: './race-item.component.html',
  styleUrls: ['./race-item.component.scss'],
})
export class RaceItemComponent implements OnInit {
  @Input() item;
  constructor() {}

  ngOnInit() {}
}
