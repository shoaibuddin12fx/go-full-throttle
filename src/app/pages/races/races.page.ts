import { Component, Injector, OnInit } from '@angular/core';
import { SELECTION_TYPE } from 'src/app/const/enums';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-races',
  templateUrl: './races.page.html',
  styleUrls: ['./races.page.scss'],
})
export class RacesPage extends BasePage implements OnInit {
  nascar_races = [];
  formula1_races = [];
  season;
  type = 1;
  name;
  constructor(injector: Injector) {
    super(injector);
  }

  async ngOnInit() {
    const { season, type } = this.nav.getQueryParams();
    this.season = season;
    this.type = type;
    console.log('TYPE IS ', type);

    if (type == SELECTION_TYPE.FORMULA_1) this.getFormula1Schedules();
    else this.getNascarSchedule();
    // let data = await this.modals.present(TypeSelectionComponent);
    // this.typeSelected(data?.data?.type);
  }

  async getFormula1Schedules() {
    let res = await this.network.getSheduleBySeason(this.season);
    console.log('getFormula1Schedules', res);
    this.name = 'Formula 1';
    this.formula1_races = res['data'];
  }

  async getNascarSchedule() {
    let res = await this.network.getNascarSchedule(this.season);
    console.log('getNascarSchedule', res);
    this.name = 'NASCAR';
    this.nascar_races =
      res?.data?.sort((a, b) => {
        return Date.parse(a.date_time) - Date.parse(b.date_time);
      }) ?? [];
    // this.races.push({
    //   name: 'NASCAR',
    //   races: res["data"],
    // });
  }

  ionViewDidEnter() {
    //  this.races = this.dataService.getRacingSeries();
    // alert(this.races.length);
  }

  viewDetail(item) {
    // let data = {
    //   item,
    //   car,
    // };
    // this.dataService.races_data = data;
    // this.nav.push('pages/car-detail');
  }

  async newsList(_type) {
    let res = await this.network.getNews(this.type);
    console.log({ res });
  }

  raceEventDetail(item) {
    this.nav.push('pages/race-detail', {
      season: item.season,
      round: item.round,
    });
  }

  nascarRaceEventDetail(item) {
    this.nav.push('pages/nascar-race-details', {
      season: item.season,
      race_id: item.race_id,
    });
  }

  gotoNewsDetail() {
    this.nav.push('pages/news-detail');
  }
}
