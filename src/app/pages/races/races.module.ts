import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RacesPageRoutingModule } from './races-routing.module';

import { RacesPage } from './races.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { HomeRaceModule } from '../home/home-race/home-race.module';
import { RaceItemModule } from './race-item/race-item.module';
import { TypeSelectionModule } from 'src/app/components/type-selection/type-selection.module';
import { NascarRaceItemComponent } from './nascar-race-item/nascar-race-item.component';
import { NascarRaceItemModule } from './nascar-race-item/nascar-race-item.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RacesPageRoutingModule,
    HeaderModule,
    RaceItemModule,
    TypeSelectionModule,
    NascarRaceItemModule
  ],
  declarations: [RacesPage],
})
export class RacesPageModule {}
