import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NascarRaceDetailsPageRoutingModule } from './nascar-race-details-routing.module';

import { NascarRaceDetailsPage } from './nascar-race-details.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { NascarDriverItemModule } from 'src/app/components/nascar-driver-item/nascar-driver-item.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NascarRaceDetailsPageRoutingModule,
    HeaderModule,
    NascarDriverItemModule
  ],
  declarations: [NascarRaceDetailsPage]
})
export class NascarRaceDetailsPageModule {}
