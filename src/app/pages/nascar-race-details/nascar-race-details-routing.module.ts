import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NascarRaceDetailsPage } from './nascar-race-details.page';

const routes: Routes = [
  {
    path: '',
    component: NascarRaceDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NascarRaceDetailsPageRoutingModule {}
