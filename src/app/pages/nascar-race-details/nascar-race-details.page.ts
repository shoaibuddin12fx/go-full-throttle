import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-nascar-race-details',
  templateUrl: './nascar-race-details.page.html',
  styleUrls: ['./nascar-race-details.page.scss'],
})
export class NascarRaceDetailsPage extends BasePage implements OnInit {
  item = {};
  title = '';
  id;
  races = [];
  positions = [];
  drivers = [];
  isPopular = false;

  constructor(injector: Injector, private route: ActivatedRoute) {
    super(injector);
  }

  ngOnInit() {
    const { race_id, season } = this.nav.getQueryParams();
    this.getData(race_id, season);
  }

  ionViewDidEnter() {
    // this.route.params.subscribe((params) => {
    //   if (params['id']) {
    //     const { id } = params;
    //     console.log(id);
    //     this.id = id;
    //     this.item = this.dataService.getRaceEventById(id);
    //     console.log(id, this.item);
    //     this.item.parent = this.dataService.getRacingSeriesById(
    //       this.item.race_id
    //     );
    //     this.positions = this.item.positions;
    //     //console.log(this.drivers);
    //     if (this.item.parent) {
    //       this.races = this.dataService.getRaceEvents(this.item.parent.id);
    //     }
    //   }
    // });
  }

  async getData(race_id, season) {
    let res = await this.network.getNascarRaceResult(race_id);
    console.log('getRaceResults -> ', res);
    this.item = res ? res['data']['Race'] : {};
    this.drivers = res ? res['data']['DriverRaces'] : [];
    if (!this.drivers?.length) this.getDrivers();
    // if (!this.item || !this.item['Results']?.length) {
    //   let _res = await this.network.getNascarSchedule(season,);
    //   console.log('getRaceSchedultByData', _res);
    //   this.item = _res && _res['0'] ? _res['data'][0] : {};
    // }
  }

  driverDetail(driver) {
    this.nav.push('pages/nascar-driver-detail');
    console.log('Driver', driver);

    this.dataService.driver_data = driver;
    this.dataService.race_data = this.item;
    this.dataService.isPopular = this.isPopular;
  }

  async getRaceDrivers(season, round) {
    let res = await this.network.getRaceDrivers(season, round);
    console.log('getRaceDrivers -> ', res);
    this.drivers = res['data'];
  }

  async getDrivers() {
    this.isPopular = true;
    let res = await this.network.getNascarDrivers();
    console.log('getNascarDrivers -> ', res);
    this.drivers = res['data'];
  }
}
