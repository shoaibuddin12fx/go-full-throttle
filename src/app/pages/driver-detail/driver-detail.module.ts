import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DriverDetailPageRoutingModule } from './driver-detail-routing.module';

import { DriverDetailPage } from './driver-detail.page';
import { HeaderModule } from 'src/app/components/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DriverDetailPageRoutingModule,
    HeaderModule,
  ],
  declarations: [DriverDetailPage],
})
export class DriverDetailPageModule {}
