import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { BasePage } from '../base-page/base-page';
import { Browser } from '@capacitor/browser';

@Component({
  selector: 'app-driver-detail',
  templateUrl: './driver-detail.page.html',
  styleUrls: ['./driver-detail.page.scss'],
})
export class DriverDetailPage extends BasePage implements OnInit {
  driver;
  result;
  drivers;
  race_event_detail;
  position;
  id;
  item: any;
  race;
  isPopular = false;
  constructor(injector: Injector, private route: ActivatedRoute) {
    super(injector);
  }

  ngOnInit() {
    this.isPopular = this.dataService.isPopular;
    this.result = this.dataService.driver_data;
    this.driver = this.dataService.driver_data?.driver;
    this.race = this.dataService.race_data;
    console.log('race', this.race);
  }

  ionViewDidEnter() {
    // this.route.params.subscribe((params) => {
    //   if (params['id']) {
    //     const { id } = params;
    //     var queryParams = this.route.snapshot.queryParams;
    //     console.log(id, queryParams);
    //     this.id = id;
    //     console.log('this.id', this.id);
    //     // this.driver = this.dataService.getDriverById(this.id);
    //     this.driver = this.dataService.getDriverData();
    //     console.log('this.drivers', this.driver);
    //     this.drivers = this.dataService.getDriversById(this.id);
    //     this.race_event_detail = this.dataService.getRaceEventById(
    //       queryParams.race_event_id
    //     );
    //     this.position = this.dataService.getPosition(
    //       queryParams.race_event_id,
    //       this.id
    //     );
    //   }
    // });
  }

  // ngOnInit() {
  //   console.log('created');

  //   let params = this.nav.getQueryParams();
  //   if (params) {
  //     const { driver_id, race_event_id } = params;
  //     this.driver = this.dataService.getDriverById(driver_id);
  //     this.drivers = this.dataService.getDriversById(driver_id);
  //     this.race_event_detail = this.dataService.getRaceEventById(race_event_id);
  //     this.position = this.dataService.getPosition(race_event_id, driver_id);
  //   }
  // }

  showDriverDetail(driver) {
    this.nav.push('pages/driver-detail/' + driver.id, {
      race_event_id: this.race_event_detail.id,
      replaceUrl: true,
    });
  }

  async gotoWeb(url) {
    await Browser.open({ url: url });
  }
}
