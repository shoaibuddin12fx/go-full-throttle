import { Component, Injector, OnInit } from '@angular/core';
import { NavService } from 'src/app/services/basic/nav.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-tabbar',
  templateUrl: './tabbar.page.html',
  styleUrls: ['./tabbar.page.scss'],
})
export class TabbarPage implements OnInit {
  constructor(private nav: NavService) {}

  ngOnInit() {}

  goToHome() {
    this.nav.setRoot('pages/tabbar/home');
  }
}
