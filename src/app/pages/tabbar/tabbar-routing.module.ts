import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabbarPage } from './tabbar.page';

const routes: Routes = [
  {
    path: '',
    component: TabbarPage,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: 'races',
        loadChildren: () =>
          import('./../races/races.module').then((m) => m.RacesPageModule),
      },
      {
        path: 'racing-news',
        loadChildren: () =>
          import('./../racing-news/racing-news.module').then(
            (m) => m.RacingNewsPageModule
          ),
      },
      {
        path: 'timetable',
        loadChildren: () =>
          import('./../timetable/timetable.module').then(
            (m) => m.TimetablePageModule
          ),
      },
      {
        path: 'videos',
        loadChildren: () =>
          import('./../videos/videos.module').then((m) => m.VideosPageModule),
      },
      {
        path: 'store',
        loadChildren: () =>
          import('./../store/store.module').then((m) => m.StorePageModule),
      },
      {
        path: 'home',
        loadChildren: () =>
          import('./../home/home.module').then((m) => m.HomePageModule),
      },
      {
        path: 'seasons',
        loadChildren: () =>
          import('./../seasons/seasons.module').then(
            (m) => m.SeasonsPageModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabbarPageRoutingModule {}
