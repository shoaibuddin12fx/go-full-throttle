import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'splash',
    pathMatch: 'full',
  },
  {
    path: 'splash',
    loadChildren: () =>
      import('./splash/splash.module').then((m) => m.SplashPageModule),
  },

  {
    path: 'signup',
    loadChildren: () =>
      import('./signup/signup.module').then((m) => m.SignupPageModule),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'races',
    loadChildren: () =>
      import('./races/races.module').then((m) => m.RacesPageModule),
  },
  {
    path: 'car-detail',
    loadChildren: () =>
      import('./car-detail/car-detail.module').then(
        (m) => m.CarDetailPageModule
      ),
  },
  {
    path: 'driver-detail',
    loadChildren: () =>
      import('./driver-detail/driver-detail.module').then(
        (m) => m.DriverDetailPageModule
      ),
  },
  {
    path: 'tabbar',
    loadChildren: () =>
      import('./tabbar/tabbar.module').then((m) => m.TabbarPageModule),
  },
  {
    path: 'news',
    loadChildren: () =>
      import('./news/news.module').then((m) => m.NewsPageModule),
  },
  {
    path: 'timetable',
    loadChildren: () =>
      import('./timetable/timetable.module').then((m) => m.TimetablePageModule),
  },
  {
    path: 'videos',
    loadChildren: () =>
      import('./videos/videos.module').then((m) => m.VideosPageModule),
  },
  {
    path: 'store',
    loadChildren: () =>
      import('./store/store.module').then((m) => m.StorePageModule),
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./profile/profile.module').then((m) => m.ProfilePageModule),
  },
  {
    path: 'notifications',
    loadChildren: () =>
      import('./notifications/notifications.module').then(
        (m) => m.NotificationsPageModule
      ),
  },
  {
    path: 'edit-profile',
    loadChildren: () =>
      import('./edit-profile/edit-profile.module').then(
        (m) => m.EditProfilePageModule
      ),
  },
  {
    path: 'race-detail',
    loadChildren: () =>
      import('./race-detail/race-detail.module').then(
        (m) => m.RaceDetailPageModule
      ),
  },
  {
    path: 'racing-news',
    loadChildren: () =>
      import('./racing-news/racing-news.module').then(
        (m) => m.RacingNewsPageModule
      ),
  },
  {
    path: 'support',
    loadChildren: () =>
      import('./support/support.module').then((m) => m.SupportPageModule),
  },
  {
    path: 'about-us',
    loadChildren: () =>
      import('./about-us/about-us.module').then((m) => m.AboutUsPageModule),
  },
  {
    path: 'terms-conditions',
    loadChildren: () =>
      import('./terms-conditions/terms-conditions.module').then(
        (m) => m.TermsConditionsPageModule
      ),
  },
  {
    path: 'reviews',
    loadChildren: () =>
      import('./reviews/reviews.module').then((m) => m.ReviewsPageModule),
  },
  {
    path: 'rating',
    loadChildren: () =>
      import('./rating/rating.module').then((m) => m.RatingPageModule),
  },
  {
    path: 'seasons',
    loadChildren: () =>
      import('./seasons/seasons.module').then((m) => m.SeasonsPageModule),
  },  {
    path: 'nascar-race-details',
    loadChildren: () => import('./nascar-race-details/nascar-race-details.module').then( m => m.NascarRaceDetailsPageModule)
  },
  {
    path: 'nascar-driver-detail',
    loadChildren: () => import('./nascar-driver-detail/nascar-driver-detail.module').then( m => m.NascarDriverDetailPageModule)
  },
  {
    path: 'news-detail',
    loadChildren: () => import('./news-detail/news-detail.module').then( m => m.NewsDetailPageModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
