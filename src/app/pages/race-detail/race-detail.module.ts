import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RaceDetailPageRoutingModule } from './race-detail-routing.module';

import { RaceDetailPage } from './race-detail.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { RaceItemModule } from '../races/race-item/race-item.module';
import { DriverItemModule } from 'src/app/components/driver-item/driver-item.module';
import { PracticeScheduleCardComponent } from './practice-schedule-card/practice-schedule-card.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RaceDetailPageRoutingModule,
    HeaderModule,
    RaceItemModule,
    DriverItemModule,
  ],
  declarations: [RaceDetailPage, PracticeScheduleCardComponent],
})
export class RaceDetailPageModule {}
