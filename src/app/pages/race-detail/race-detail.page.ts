import { Component, Injector, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-race-detail',
  templateUrl: './race-detail.page.html',
  styleUrls: ['./race-detail.page.scss'],
})
export class RaceDetailPage extends BasePage implements OnInit {
  item = {};
  title = '';
  id;
  races = [];
  positions = [];
  drivers = [];
  isPopular = false;

  constructor(injector: Injector, private route: ActivatedRoute) {
    super(injector);
  }

  ngOnInit() {
    const { season, round } = this.nav.getQueryParams();
    this.getData(season, round);
  }

  ionViewDidEnter() {
    // this.route.params.subscribe((params) => {
    //   if (params['id']) {
    //     const { id } = params;
    //     console.log(id);
    //     this.id = id;
    //     this.item = this.dataService.getRaceEventById(id);
    //     console.log(id, this.item);
    //     this.item.parent = this.dataService.getRacingSeriesById(
    //       this.item.race_id
    //     );
    //     this.positions = this.item.positions;
    //     //console.log(this.drivers);
    //     if (this.item.parent) {
    //       this.races = this.dataService.getRaceEvents(this.item.parent.id);
    //     }
    //   }
    // });
  }

  async getData(season, round) {
    let res = await this.network.getRaceResults(season, round);
    console.log('getRaceResults -> ', res);
    this.item = res ? res['data'][0] : {};
    if (!this.item || !this.item['Results']?.length) {
      // let _res = await this.network.getRaceSchedultByData(season, round);
      // console.log('getRaceSchedultByData', _res);
      // this.item = _res && _res['0'] ? _res['data'][0] : {};
      this.getDrivers();
    }
  }

  driverDetail(driver) {
    this.nav.push('pages/driver-detail');
    this.dataService.driver_data = driver;
    this.dataService.race_data = this.item;
    this.dataService.isPopular = this.isPopular;
  }

  // async getRaceDrivers(season, round) {
  //   let res = await this.network.getRaceDrivers(season, round);
  //   console.log('getRaceDrivers -> ', res);
  //   this.drivers = res['data'];
  // }

  async getDrivers() {
    this.isPopular = true;
    let res = await this.network.getDrivers();
    console.log('getDrivers -> ', res);
    this.drivers = res['data']?.map((x) => ({
      driver: x,
    }));
  }
}
