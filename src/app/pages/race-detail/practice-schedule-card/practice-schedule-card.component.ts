import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'practice-schedule-card',
  templateUrl: './practice-schedule-card.component.html',
  styleUrls: ['./practice-schedule-card.component.scss'],
})
export class PracticeScheduleCardComponent implements OnInit {
  @Input() item; 
  @Input() title;

  constructor() { }

  ngOnInit() {}

}
