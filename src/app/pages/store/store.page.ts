import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-store',
  templateUrl: './store.page.html',
  styleUrls: ['./store.page.scss'],
})
export class StorePage implements OnInit {
  list = [];
  constructor() {}

  ngOnInit() {
    for (var i = 1; i < 100; i++) {
      this.list.push({
        content: 'Lorem Ipsum',
      });
    }
  }
}
