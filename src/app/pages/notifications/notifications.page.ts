import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage extends BasePage implements OnInit {
  list = [];
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.getNotifications();
    // for (var i = 1; i < 100; i++) {
    //   this.list.push({
    //     content: 'Lorem Ipsum',
    //   });
    // }
  }

  async getNotifications() {
    let res = await this.network.getAppNotifications();
    this.list = res?.data ?? [];
  }
}
