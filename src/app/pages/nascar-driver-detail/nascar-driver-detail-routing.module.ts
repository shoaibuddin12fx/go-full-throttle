import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NascarDriverDetailPage } from './nascar-driver-detail.page';

const routes: Routes = [
  {
    path: '',
    component: NascarDriverDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NascarDriverDetailPageRoutingModule {}
