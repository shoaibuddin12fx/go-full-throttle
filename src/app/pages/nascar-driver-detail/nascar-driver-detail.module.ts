import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NascarDriverDetailPageRoutingModule } from './nascar-driver-detail-routing.module';

import { NascarDriverDetailPage } from './nascar-driver-detail.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { HeaderModule } from 'src/app/components/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NascarDriverDetailPageRoutingModule,
    HeaderModule,
  ],
  declarations: [NascarDriverDetailPage],
})
export class NascarDriverDetailPageModule {}
