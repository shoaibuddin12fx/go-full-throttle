import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'nascar-driver-detail',
  templateUrl: './nascar-driver-detail.page.html',
  styleUrls: ['./nascar-driver-detail.page.scss'],
})
export class NascarDriverDetailPage extends BasePage implements OnInit {
  driver;
  result;
  drivers;
  race_event_detail;
  position;
  id;
  item: any;
  race;
  isPopular = false;

  constructor(injector: Injector, private route: ActivatedRoute) {
    super(injector);
  }

  ngOnInit() {
    this.isPopular = this.dataService.isPopular;
    this.driver = this.dataService.driver_data;
    this.race = this.dataService.race_data;
    console.log('driver', this.driver);
  }

  showDriverDetail(driver) {
    this.nav.push('pages/driver-detail/' + driver.id, {
      race_event_id: this.race_event_detail.id,
      replaceUrl: true,
    });
  }
}
