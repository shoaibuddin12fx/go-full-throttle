import { Component, Injector, OnInit } from '@angular/core';
import { ViewDidEnter } from '@ionic/angular';
import { ImageService } from 'src/app/services/image.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage extends BasePage implements OnInit, ViewDidEnter {
  user = {};
  races = [];
  isOwnProfile: any;
  profilePhoto = 'assets/user.png';
  image_data;

  constructor(injector: Injector, public image: ImageService) {
    super(injector);
  }

  async ngOnInit() {
    this.user = await this.users.getLocalUser();
    if (this.user['profile_image'])
      this.profilePhoto = `http://gft.waprojects.space/images/${this.user['profile_image']}`;
  }

  ionViewDidEnter() {
    this.initialize();
    if (this.user['profile_image'])
      this.profilePhoto = `http://gft.waprojects.space/images/${this.user['profile_image']}`;
  }

  async initialize() {
    // this.user = await this.users.getUser();
    this.races = await this.dataService.getRacingSeries();
  }

  async onSave() {
    console.log('SavedUser', this.user);
    let res = await this.network.updateProfile(this.user);
    console.log('updateProfile', res);

    if (res && res.data) {
      this.users.setLocalUser(res.data.user);
      this.utility.presentSuccessToast(res.message);
      this.events.publish('USER_DATA_UPDATED');
      console.log('Event Published');
      this.nav.pop();
    } else
      this.utility.presentFailureToast(
        res?.error.message ?? 'Something went wrong'
      );
    // let user = await this.firebase.getUser();
    // this.firebase.SetUserData(user, this.user);
    // this.utility.presentSuccessToast('Success');
    // this.dataService.user = this.user;
  }

  async changeProfile() {
    // if (!this.isOwnProfile) {
    //   return;
    // }

    console.log('Here');

    const res = await this.doGetPicture();
    if (res) {
      this.utility.presentSuccessToast('Image Uploaded');
    }
  }

  async doGetPicture() {
    return new Promise(async (resolve) => {
      let _img = await this.image.openCamera();
      console.log('IMAGE_DATA', _img);

      // let blob = await this.image.b64toBlob(
      //   _img['base64String'],
      //   'image/' + _img['format']
      // );
      // console.log(blob);
      if (_img && _img['base64String'])
        this.imageReceived(_img['base64String']);
      // resolve(res);
    });
  }

  imageReceived(b64) {
    this.profilePhoto = `data:image/png;base64,${b64}`;
    this.user['profile_image'] = b64;
  }
}
