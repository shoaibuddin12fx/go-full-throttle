import { Component, Injector, OnInit } from '@angular/core';
import { PromotionsComponent } from 'src/app/components/promotions/promotions.component';
import { VideoPlayer } from '@ionic-native/video-player/ngx';
import { ModalService } from 'src/app/services/basic/modal.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage extends BasePage implements OnInit {
  constructor(
    public modals: ModalService,
    injector: Injector,
    private videoPlayer: VideoPlayer
  ) {
    super(injector);
  }

  slides = [];
  news = [];
  races = [];

  ngOnInit() {
    let params = this.nav.getQueryParams();
    console.log('PARAMS', params);
    this.getNews(params.type);
  }

  async getNews(type) {
    let res = await this.network.getNews(type);
    console.log('getNews', res);
    this.news = res?.data ?? [];
  }

  ionViewDidEnter() {
    // this.modals.present(PromotionsComponent, {}, 'transparent-banner');
    // this.slides = this.dataService.getHomeSlides();
    // this.news = this.dataService.getNews();
    // this.races = this.dataService.getRacingSeries();
  }

  raceEventDetail(id) {
    this.nav.push('pages/tabbar/races');
  }

  watchVideo() {
    this.videoPlayer
      .play('https://www.youtube.com/watch?v=bHWgc5MPnPA')
      .then(() => {
        console.log('video completed');
      })
      .catch((err) => {
        console.log(err);
      });
  }
  gotoNewsDetail(item) {
    this.dataService.news_data = item;
    this.nav.push('pages/news-detail');
  }
}
