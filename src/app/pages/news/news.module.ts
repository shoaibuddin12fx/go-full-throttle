import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewsPageRoutingModule } from './news-routing.module';

import { NewsPage } from './news.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { HomeNewsModule } from '../home/home-news/home-news.module';
import { HomeRaceModule } from '../home/home-race/home-race.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeNewsModule,
    HomeRaceModule,
    HeaderModule,
    NewsPageRoutingModule,
  ],
  declarations: [NewsPage],
})
export class NewsPageModule {}
