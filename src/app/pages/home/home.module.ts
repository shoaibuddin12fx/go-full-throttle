import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { PromotionsModule } from 'src/app/components/promotions/promotions.module';
import { HomeNewsModule } from './home-news/home-news.module';
import { HeaderModule } from 'src/app/components/header/header.module';
import { HomeRaceModule } from './home-race/home-race.module';
import { TypeSelectionComponent } from 'src/app/components/type-selection/type-selection.component';
import { TypeSelectionModule } from 'src/app/components/type-selection/type-selection.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    PromotionsModule,
    HomeNewsModule,
    HomeRaceModule,
    HeaderModule,
    TypeSelectionModule,
  ],
  declarations: [HomePage],
})
export class HomePageModule {}
