import { Component, Input, OnInit } from '@angular/core';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'app-home-race',
  templateUrl: './home-race.component.html',
  styleUrls: ['./home-race.component.scss'],
})
export class HomeRaceComponent implements OnInit {
  @Input() item;

  constructor(private network: NetworkService) {}

  ngOnInit(): void {}
}
