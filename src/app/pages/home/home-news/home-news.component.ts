import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-home-news',
  templateUrl: './home-news.component.html',
  styleUrls: ['./home-news.component.scss'],
})
export class HomeNewsComponent extends BasePage implements OnInit {
  @Input() item;
  constructor(injector: Injector) {
    super(injector)
  }

  ngOnInit() {}

  openDetails(){
    console.log("news");
    this.nav.push("pages/news")
  }
}
