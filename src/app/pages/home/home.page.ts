import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { PromotionsComponent } from 'src/app/components/promotions/promotions.component';
import { TypeSelectionComponent } from 'src/app/components/type-selection/type-selection.component';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage extends BasePage implements OnInit {
  slides = [];
  news = [];
  races = [];
  recentSchedules = [];
  nascarRecentSchedule = [];
  list = ['1', '2', '3'];
  constructor(injector: Injector) {
    super(injector);
  }
  // @ViewChild('tabs', { static: false }) tabs: IonTabs;
  // static selectedTab;

  async ngOnInit() {
    // let res = await this.network.getConstructors();
    // console.log('getConstructors -> ', res);
  }

  setCurrentTab() {
    // HomePage.selectedTab = this.tabs.getSelected();
  }

  ionViewDidEnter() {
    console.log(localStorage.getItem('showStartModal'));
    if (localStorage.getItem('showStartModal') != 'false') {
      setTimeout(() => {
        this.modals.present(PromotionsComponent, {}, 'transparent-banner');
      }, 2000);
    }
    this.slides = this.dataService.getHomeSlides();
    console.log('home detail', this.slides);

    this.news = this.dataService.getNews();
    this.races = this.dataService.getRacingSeries();
    this.getRecentSchedule();
    this.getNascarSchedule();
  }

  async getNascarSchedule() {
    let res = await this.network.getNascarRecentSchedule();
    this.nascarRecentSchedule = res.data?.map((item) => ({
      ...item,
      image: `assets/nascar/n_${Math.floor(Math.random() * 5 + 1)}.jpg`,
    }));
  }
  async getRecentSchedule() {
    let res = await this.network.getRecentSchedule();
    console.log('getRecentSchedule', res);
    this.recentSchedules = res.data?.map((item) => ({
      ...item,
      image: `assets/f1/f1_${Math.floor(Math.random() * 5 + 1)}.jpg`,
    }));
  }

  // raceEventDetail(item) {
  //   this.nav.push('pages/race-detail', {
  //     season: item.season,
  //     round: item.round,
  //   });
  // }

  nascarRaceEventDetail(item) {
    this.nav.push('pages/nascar-race-details', {
      season: item.season,
      race_id: item.race_id,
    });
  }

  raceEvent(item) {
    console.log('raceEvent', item);

    this.nav.push('pages/race-detail', {
      season: item.season,
      round: item.round,
    });
  }

  openCarDetails(id) {
    this.nav.push('pages/race-detail/' + id);
  }

  raceEventDetail(id) {
    this.nav.push(`pages/news`, { type: id });
  }
}
