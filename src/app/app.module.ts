import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { UtilityService } from './services/utility.service';

import { FormBuilder } from '@angular/forms';
// import { WebView } from '@awesome-cordova-plugins/ionic-webview/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './services/interceptor.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { VideoPlayer } from '@ionic-native/video-player/ngx';
import { GooglePlus } from '@awesome-cordova-plugins/google-plus/ngx';
// import { FirebaseService } from './services/firebase.service';
// import { AngularFireModule } from '@angular/fire/compat';
// import { environment } from 'src/environments/environment';
// import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
// import {
//   provideAuth,
//   initializeAuth,
//   indexedDBLocalPersistence,
//   getAuth,
// } from '@angular/fire/auth';
import { Capacitor } from '@capacitor/core';
import { getApp } from 'firebase/app';
import { ImageService } from './services/image.service';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
    // AngularFireModule.initializeApp(environment.firebaseConfig),
    // AngularFireDatabaseModule,
    // provideAuth(() => {
    //   if (Capacitor.isNativePlatform()) {
    //     return initializeAuth(getApp(), {
    //       persistence: indexedDBLocalPersistence,
    //     });
    //   } else {
    //     return getAuth();
    //   }
    // }),
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    VideoPlayer,
    FormBuilder,
    UtilityService,
    //  WebView,
    InAppBrowser,
    UtilityService,
    Geolocation,
    LaunchNavigator,
    GooglePlus,
    ImageService,
  
    // FirebaseService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
