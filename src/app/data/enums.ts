export enum TYPES {
  NASCAR = 1,
  FORMULA_1 = 2,
  OUTLAWS = 3,
  INDYCAR = 4,
}
