import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TypeSelectionComponent } from './type-selection.component';
import { HeaderModule } from '../header/header.module';

@NgModule({
  declarations: [TypeSelectionComponent],
  imports: [CommonModule, IonicModule, HeaderModule],
  exports: [TypeSelectionComponent],
})
export class TypeSelectionModule {}
