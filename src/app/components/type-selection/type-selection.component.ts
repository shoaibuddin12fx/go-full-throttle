import { Component, Injector, Input, OnInit } from '@angular/core';
import { SELECTION_TYPE } from 'src/app/const/enums';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-type-selection',
  templateUrl: './type-selection.component.html',
  styleUrls: ['./type-selection.component.scss'],
})
export class TypeSelectionComponent extends BasePage implements OnInit {
  list = [
    {
      title: 'Formula 1',
      type: SELECTION_TYPE.FORMULA_1,
      image: 'assets/icon/f1.png',
    },
    {
      title: 'NASCAR',
      type: SELECTION_TYPE.NASCAR,
      image: 'assets/icon/nascar.jpg',
    },
  ];
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  typeSelected(type) {
    this.modals.dismiss({ type });
  }
}
