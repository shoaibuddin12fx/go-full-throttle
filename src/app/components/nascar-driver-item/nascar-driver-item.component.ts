import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'nascar-driver-item',
  templateUrl: './nascar-driver-item.component.html',
  styleUrls: ['./nascar-driver-item.component.scss'],
})
export class NascarDriverItemComponent extends BasePage implements OnInit {
  _item;
  @Input() item;
  @Input() isPopular;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}
}
