import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NascarDriverItemComponent } from './nascar-driver-item.component';

@NgModule({
  declarations: [NascarDriverItemComponent],
  imports: [CommonModule, IonicModule],
  exports: [NascarDriverItemComponent],
})
export class NascarDriverItemModule {}
