import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { NavService } from 'src/app/services/basic/nav.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent extends BasePage implements OnInit {
  @Input() title = '';
  @Input() searchVisible = false;
  @Input() back = true;
  @Input() showcurve = true;
  user;
  profilePhoto: string;
  static headers = [];
  image_url = 'http://gft.waprojects.space/images/';

  constructor(public nav: NavService, injector: Injector) {
    super(injector);
    HeaderComponent.headers.push(this);
    this.events.subscribe('USER_DATA_UPDATED', async () => {
      console.log('USER_DATA_UPDATED');
      let user = await this.users.getLocalUser();
      if (user['profile_image'])
        this.profilePhoto = `${this.image_url}${user['profile_image']}`;
    });
  }

  async ngOnInit() {
    console.log('HeaderComponent');

    // console.log(this.events.subscriptions);

    this.user = await this.users.getLocalUser();
    if (this.user['profile_image'])
      this.profilePhoto = `${this.image_url}${this.user['profile_image']}`;
  }

  // ngOnInit() {}

  async goBack() {
    let overlay = await this.modals.getOverLay();
    if (overlay) this.modals.dismiss({});
    else this.nav.pop();
  }

  gotoProfile() {
    this.nav.push('pages/profile');
  }

  gotoNotifications() {
    this.nav.push('pages/notifications');
  }
}
