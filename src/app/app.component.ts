import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, ModalController, Platform } from '@ionic/angular';
import { NavService } from './services/basic/nav.service';
import { UserService } from './services/user.service';
import { UtilityService } from './services/utility.service';
// import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
} from '@capacitor/push-notifications';
import { Plugins } from '@capacitor/core';

const { LocalNotifications } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  isModalOpen;
  zone: any;
  firebaseService: any;
  constructor(
    public platform: Platform,
    public utility: UtilityService,
    private modalController: ModalController,
    private router: Router
  ) {
    localStorage.removeItem('showStartModal');

    platform.ready().then(() => {
      // menuCtrl.enable(false, 'main');
      // set default url from app side
      this.beInitialize();
    });
  }

  beInitialize() {
    // GoogleAuth.initialize();
    if (
      // this.platform.is('cordova') ||
      this.platform.is('ios')
      // this.platform.is('android')
    ) {
      window['gapi'] = {
        load: (name: string) => Promise.resolve(),
        iframes: {
          getContext: () => {
            return {
              iframe: {
                contentWindow: {
                  postMessage: (message: any) => {
                    console.log('gapi iframe message:', message);
                  },
                },
              },
            };
          },
        },
      } as any;
    }

    // this.platform.backButton.subscribeWithPriority(1, () => {
    //   console.log('hit back btn');
    // });

    // this.router.events.subscribe(async () => {
    //   // if (router.url.toString() === "/tabs/home" && isModalOpened) this.modalController.dismiss();
    // });

    document.addEventListener(
      'backbutton',
      (event) => {
        event.preventDefault();
        event.stopPropagation();
        const url = this.router.url;
        console.log(url);
        this.createBackRoutingLogics(url);
      },
      false
    );
  }

  async createBackRoutingLogics(url) {
    if (
      url.includes('login') ||
      url.includes('signup') ||
      url.includes('dashboard') ||
      url.includes('tutorial')
    ) {
      this.utility.hideLoader();

      const isModalOpen = await this.modalController.getTop();
      console.log(isModalOpen);
      if (isModalOpen) {
        this.modalController.dismiss({ data: 'A' });
      } else {
        this.exitApp();
      }
    } else {
      if (this.isModalOpen) {
      }
    }
  }

  exitApp() {
    navigator['app'].exitApp();
  }

    firebaseInit() {
    console.log('Initializing firebase');
    this.firebaseService.fireAuth.onAuthStateChanged((user) => {
      console.log('OnFireAuthChanged', user);
      if (user) {
        // this.firebase.grantPermission();
        console.log(user);
        this.zone.run(() => {
          console.log('Page change requested');
          // this.nav.push('pages/locationloader');
        });
      }
    });

    // Request permission to use push notifications
    // iOS will prompt user and return if they granted permission or not
    // Android will just grant without prompting
    PushNotifications.requestPermissions().then((result) => {
      if (result.receive === 'granted') {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
      }
    });

    // On success, we should be able to receive notifications
    PushNotifications.addListener('registration', (token: Token) => {
      console.log('FCM_TOKEN', token.value);
      localStorage.setItem('FCM_TOKEN', token.value);
      // alert('Push registration success, token: ' + token.value);
    });

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError', (error: any) => {
      alert('Error on registration: ' + JSON.stringify(error));
    });

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener(
      'pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        this.showNotification(notification.title, notification.body);
        //  this.utility.alerts.showAlert(notification.body, notification.title);
        //alert('Push received: ' + JSON.stringify(notification));
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification: ActionPerformed) => {
        alert('Push action performed: ' + JSON.stringify(notification));
      }
    );
  }
  // showNotification(title: string, body: string) {
  //   throw new Error('Method not implemented.');
  // }
  async showNotification(title, body) {
    const notifs = await LocalNotifications.schedule({
      notifications: [
        {
          title,
          body,
          id: 1,
          schedule: { at: new Date(Date.now() + 1000) },
          sound: 'default',
          attachments: null,
          actionTypeId: '',
          extra: null,
        },
      ],
    });
    console.log('scheduled notifications', notifs);
  }
}
